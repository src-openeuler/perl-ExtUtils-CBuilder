Name:           perl-ExtUtils-CBuilder
Epoch:          1
Version:        0.280236
Release:        2
Summary:        Build the C portions of Perl modules
License:        GPL-1.0-or-later or Artistic-1.0-Perl
URL:            https://metacpan.org/release/ExtUtils-CBuilder
Source0:        https://cpan.metacpan.org/authors/id/A/AM/AMBS/ExtUtils-CBuilder-%{version}.tar.gz
# Please note that Patch0 is from mageia, which will put libperl.so into extra_linker_flags.
# So it is quite different from redhat/fedora, which put libperl.so into lddlflags
Patch0:         ExtUtils-CBuilder-0.280236-Link-XS-modules-to-libperl.so-with-EU-CBuilder-on-Li.patch
BuildArch:      noarch
BuildRequires:  make perl-generators perl-interpreter perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict) perl(warnings) perl(Config) perl(Cwd) perl(DynaLoader)
BuildRequires:  perl(File::Basename) perl(File::Path) perl(File::Spec) >= 3.13
BuildRequires:  perl(File::Temp) perl(IPC::Cmd) perl(Perl::OSType) >= 1
BuildRequires:  perl(Text::ParseWords) perl-devel gcc-c++ perl(Test::More) >= 0.47
Requires:       gcc-c++
Requires:       perl(DynaLoader) perl(ExtUtils::Mksymlists) >= 6.30 perl(File::Spec) >= 3.13
Requires:       perl(Perl::OSType) >= 1 perl-devel

%{?perl_default_filter}
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\((File::Spec|Perl::OSType)\\)$

%description
This module can build the C portions of Perl modules by invoking the
appropriate compilers and linkers in a cross-platform manner. It was
motivated by the 'Module::Build' project, but may be useful for other
purposes as well. However, it is _not_ intended as a general cross-platform
interface to all your C building needs. That would have been a much more
ambitious goal!

%package help
Summary: Help documents for perl-ExtUtils-CBuilder

%description help
This package provides man pages and other related help documents for perl-ExtUtils-CBuilder.

%prep
%autosetup -n ExtUtils-CBuilder-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/*

%files help
%doc Changes CONTRIBUTING README README.mkdn
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 1:0.280236-2
- Link XS modules to libperl.so with EU::CBuilder on Linux
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 1:0.280236-1
- Upgrade to version 0.280236

* Wed Nov 27 2019 yanzhihua <yanzhihua4@huawei.com> - 1:0.280230-418
- Package init
